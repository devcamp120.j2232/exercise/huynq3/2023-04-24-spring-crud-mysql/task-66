package com.devcamp.task62jpaprovincerelationship.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62jpaprovincerelationship.model.Ward;
import com.devcamp.task62jpaprovincerelationship.repository.IWardRepository;

@Service
public class WardService {
    @Autowired
    IWardRepository wardRepository;
    public ArrayList<Ward> getAllWards(){
        ArrayList<Ward> wardList = new ArrayList<>();
        wardRepository.findAll().forEach(wardList:: add);
        return wardList;
    }
}
