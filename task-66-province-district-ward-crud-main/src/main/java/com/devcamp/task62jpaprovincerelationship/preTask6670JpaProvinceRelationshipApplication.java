package com.devcamp.task62jpaprovincerelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class preTask6670JpaProvinceRelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(preTask6670JpaProvinceRelationshipApplication.class, args);
	}

}
