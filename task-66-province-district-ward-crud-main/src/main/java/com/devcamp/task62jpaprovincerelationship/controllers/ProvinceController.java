package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.Province;
import com.devcamp.task62jpaprovincerelationship.repository.IProvinceRepository;
import com.devcamp.task62jpaprovincerelationship.services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/province")
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;
    
    //get all provinces list
    @GetMapping("/all")
    public ResponseEntity<List<Province>> getAllProvinces(){
        try {
            return new ResponseEntity<>(provinceService.getAllProvinces(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get province detail by id
    @Autowired
    IProvinceRepository provinceRepository;
    
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getProvinceById(@PathVariable int id){
        Province province = provinceRepository.findById(id);
        if (province != null){
            return new ResponseEntity<>(province, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //create new province
    @PostMapping("/create")
    public ResponseEntity<Object> createProvince(@RequestBody Province pProvince){
        try {
            Province _province = provinceRepository.save(pProvince);
            return new ResponseEntity<>(_province, HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update province
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProvince(@PathVariable(name="id")int id, @RequestBody Province pProvinceUpdate){
        try {
           Province province = provinceRepository.findById(id);
            if (province != null){
                province.setCode(pProvinceUpdate.getCode());
                province.setName(pProvinceUpdate.getName());
                
                return new ResponseEntity<>(provinceRepository.save(province), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete province by Id
     @DeleteMapping("/delete/{id}")
     public ResponseEntity<Province> deleteProvinceById(@PathVariable("id") int id) {
         try {
            Province province = provinceRepository.findById(id); 
            provinceRepository.delete(province);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
 

}
