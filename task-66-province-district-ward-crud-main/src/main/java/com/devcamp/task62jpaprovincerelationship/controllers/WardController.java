package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Ward;
import com.devcamp.task62jpaprovincerelationship.repository.IDistrictRepository;
import com.devcamp.task62jpaprovincerelationship.repository.IWardRepository;
import com.devcamp.task62jpaprovincerelationship.services.DistrictService;
import com.devcamp.task62jpaprovincerelationship.services.WardService;

@RestController
@RequestMapping("/ward")
@CrossOrigin
public class WardController {
    @Autowired
    DistrictService districtService;
    @Autowired
    WardService wardService;
    
    //get all wards lisst
    @GetMapping("/all")
    public ResponseEntity<List<Ward>> getAllWards(){
        try {
            return new ResponseEntity<>(wardService.getAllWards(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get wards by district id
    @GetMapping("/search")
    public ResponseEntity<Set<Ward>> getWardsByDistrictIdApi(@RequestParam(value = "districtId") int id){
        try {
            Set<Ward> provinceDistricts = districtService.getWardsByDistrictId(id);
            if (provinceDistricts != null ){
                return new ResponseEntity<>(provinceDistricts, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //get district detail by id
    @Autowired
    IWardRepository wardRepository;
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable(name="id") int id){
        Ward ward = wardRepository.findById(id);
        if (ward != null){
            return new ResponseEntity<>(ward, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //create new ward with district id
    @Autowired
    IDistrictRepository districtRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createWard(@PathVariable("id") int id, @RequestBody Ward pWard){
        try {
            District district = districtRepository.findById(id);
            if (district !=null){
                Ward newWard = new Ward();
                newWard.setName(pWard.getName());
                newWard.setPrefix(pWard.getPrefix());
                newWard.setDistrict(district);
                Ward _ward = wardRepository.save(newWard);
                //Ward _ward = wardRepository.save(pWard);
                return new ResponseEntity<>(_ward, HttpStatus.CREATED);
            } else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update ward
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateWard(@PathVariable(name="id")int id, @RequestBody Ward pWardUpdate){
        try {
           Ward ward = wardRepository.findById(id);
            if (ward != null){
                ward.setName(pWardUpdate.getName());
                ward.setPrefix(pWardUpdate.getPrefix());
                ward.setDistrict(pWardUpdate.getDistrict());
                
                return new ResponseEntity<>(wardRepository.save(ward), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete ward by Id
     @DeleteMapping("/delete/{id}")
     public ResponseEntity<Ward> deleteWardById(@PathVariable("id") int id) {
         try {
            Ward ward = wardRepository.findById(id);
            wardRepository.delete(ward);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
}
