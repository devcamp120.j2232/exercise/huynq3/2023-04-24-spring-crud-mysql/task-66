package com.devcamp.task62jpaprovincerelationship.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Long> {
    Ward findById(int id);
    List<Ward> findByDistrictId(int id);
    Optional<District> findByIdAndDistrictId(Long id, Long instructorId);
}
