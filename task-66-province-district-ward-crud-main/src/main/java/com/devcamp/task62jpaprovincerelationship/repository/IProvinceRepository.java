package com.devcamp.task62jpaprovincerelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.Province;

public interface IProvinceRepository extends JpaRepository<Province, Long>{
    Province findById(int id);
    
}
